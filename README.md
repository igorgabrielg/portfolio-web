<h4 align="center"> 
	Portfólio Web
</h4>
<p align="center">
  <a>
  <img alt="GitHub top language" src="https://img.shields.io/github/languages/top/igorgabrielg/portfolio-web?color=green">
  </a>
  
  <a>
  <img alt="GitHub repo size" src="https://img.shields.io/github/repo-size/igorgabrielg/portfolio-web?color=green">
  </a>
  
  <a href="https://www.linkedin.com/in/igorgabrielg/">
    <img alt="Made by Igor Gabriel" src="https://img.shields.io/badge/made%20by-Igor Gabriel-%2304D361?color=green">
  </a>

  <a href="https://github.com/igorgabrielg/Omnistack11/commits/master">
    <img alt="GitHub last commit" src="https://img.shields.io/github/last-commit/igorgabrielg/portfolio-web?color=green">
  </a>
  <a>
  <img alt="License" src="https://img.shields.io/badge/license-MIT-bright?color=green">
  </a>
  
  <br>
  
</p>
<p align="center">
  <a href="#bulb-projeto">Projeto</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#-imagens">Imagens</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#rocket-tecnologias">Tecnologias</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#-como-contribuir">Como contribuir</a>
</p>


## :bulb: Projeto
Esse site apresenta minha carreira profissional, nele é possivel ver um linha tempo que mostra minhas decisões e evolução de cada ano.
E possivel ver meus projetos e artigos de tecnologias. 

Link: [Portfólio Web](http://igorgabriell.000webhostapp.com/)

## 🔖 Imagens

- **Web**

<img src="assets/img/portifolio12.PNG" width="700px" >
<br>
<img src="assets/img/portifolio9.PNG" width="700px" >

- **Mobile**

<img src="assets/img/portifolio8.jpeg" width="220px" >
&nbsp;&nbsp;&nbsp;
<img src="assets/img/portifolio7.jpeg" width="220px" >
&nbsp;&nbsp;&nbsp;
<img src="assets/img/portifolio6.jpeg" width="220px" >



## :rocket: Tecnologias

Esse projeto esta sendo desenvolvida com as seguintes tecnologias:

- HTML
- JS
- CSS
- [BootStrap](https://getbootstrap.com/)

## 🤔 Como contribuir

- Faça um fork desse repositório;
- Cria uma branch com a sua feature: `git checkout -b minha-feature`;
- Faça commit das suas alterações: `git commit -m 'feat: Minha nova feature'`;
- Faça push para a sua branch: `git push origin minha-feature`.

Depois que o merge da sua pull request for feito, você pode deletar a sua branch.